package challenge.exception;

public class ForbiddenException extends RuntimeException {

    public ForbiddenException(Long id) {
        super("Forbidden access to subscription '" + id + "'");
    }
}
