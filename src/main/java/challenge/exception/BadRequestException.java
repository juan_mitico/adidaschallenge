package challenge.exception;

public class BadRequestException extends RuntimeException {

    public BadRequestException() {
        super("Fields 'email', 'birthDate', 'newsletterId' and 'consent' are mandatory");
    }
}
