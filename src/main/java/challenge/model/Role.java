package challenge.model;

public enum Role {
    USER,
    ADMIN
}
