package challenge.model;

public enum Gender {
    MALE,
    FEMALE
}
