package challenge.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(uniqueConstraints={@UniqueConstraint(columnNames={"email", "newsletterId"})})
public class Subscription {

    @Id @GeneratedValue
    private Long id;

    public String email;

    public String firstName;

    public Gender gender;

    @JsonFormat(pattern = "dd-MM-yyyy")
    public Date birthDate;

    public boolean consent;

    public String newsletterId;

    @JsonIgnore @ManyToOne @JoinColumn(name = "userId", nullable = false)
    private ApiUser user;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public Gender getGender() {
        return gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public boolean isConsent() {
        return consent;
    }

    public String getNewsletterId() {
        return newsletterId;
    }

    public ApiUser getUser() {
        return user;
    }

    public void setUser(ApiUser user) {
        this.user = user;
    }

    public Subscription(String email, String firstName, Gender gender, Date birthDate, boolean consent, String newsletterId, ApiUser user) {
        this.email = email;
        this.firstName = firstName;
        this.gender = gender;
        this.birthDate = birthDate;
        this.consent = consent;
        this.newsletterId = newsletterId;
        this.user = user;
    }

    public Subscription() {
    }

    @Override
    public String toString() {
        return "Subscription{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", gender=" + gender +
                ", birthDate=" + birthDate +
                ", consent=" + consent +
                ", newsletterId='" + newsletterId + '\'' +
                ", user=" + user +
                '}';
    }
}
