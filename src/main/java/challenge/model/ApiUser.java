package challenge.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class ApiUser {

    @Id @GeneratedValue
    private Long id;

    @Column(unique = true)
    public String username;

    @JsonIgnore
    public String password;

    @Column(nullable = false)
    public Role role;

    @OneToMany(mappedBy = "user")
    private Set<Subscription> subscriptions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }

    public Set<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public ApiUser(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public ApiUser() {
    }
}
