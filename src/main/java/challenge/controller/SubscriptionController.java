package challenge.controller;

import challenge.converter.GenderEnumConverter;
import challenge.exception.BadRequestException;
import challenge.exception.ForbiddenException;
import challenge.exception.SubscriptionNotFoundException;
import challenge.exception.UserNotFoundException;
import challenge.model.ApiUser;
import challenge.model.Gender;
import challenge.model.Subscription;
import challenge.repository.SubscriptionRepository;
import challenge.repository.ApiUserRepository;
import challenge.resource.SubscriptionResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class SubscriptionController {

    private final SubscriptionRepository subscriptionRepository;
    private final ApiUserRepository apiUserRepository;

    @Autowired
    public SubscriptionController(SubscriptionRepository subscriptionRepository, ApiUserRepository apiUserRepository) {
        this.subscriptionRepository = subscriptionRepository;
        this.apiUserRepository = apiUserRepository;
    }

    @RequestMapping(value = "/subscriptions", method = RequestMethod.POST,
            produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> subscribe(Principal principal, @RequestBody Subscription subscription) {
        validateSubscriptionParam(subscription);

        subscription.setUser(getUser(principal.getName()));
        subscriptionRepository.save(subscription);

        // subscription variable has been updated when saving data to DB, so now it contains all data, including ID.
        Link subscriptionLink = new SubscriptionResource(subscription).getLink("self");
        return ResponseEntity.created(URI.create(subscriptionLink.getHref())).body(subscription.getId());
    }

    @RequestMapping(value = "/subscriptions", method = RequestMethod.GET, produces = "application/json")
    public Resources<SubscriptionResource> getAllSubscriptions(Principal principal) {
        List<SubscriptionResource> subscriptions = subscriptionRepository.findByUser_Username(principal.getName())
                .stream().map(SubscriptionResource::new).collect(Collectors.toList());
        return new Resources<>(subscriptions);
    }

    @RequestMapping(value = "/subscriptions/{id}", method = RequestMethod.GET, produces = "application/json")
    public SubscriptionResource getSubscription(Principal principal,  @PathVariable Long id) {
        Subscription subscription = validateSubscriptionAccess(principal, id);
        return new SubscriptionResource(subscription);
    }

    @RequestMapping(value = "/subscriptions/{id}", method = RequestMethod.PUT,
            produces = "application/json", consumes = "application/json")
    public ResponseEntity<?> updateSubscription(Principal principal,  @PathVariable Long id, @RequestBody Subscription subscription) {
        validateSubscriptionParam(subscription);

        // To avoid creation if subscription does not exist and returning the appropriate status code.
        validateSubscriptionAccess(principal, id);

        // Id specified in subscription parameter is ignored
        subscription.setId(id);
        subscription.setUser(getUser(principal.getName()));
        subscriptionRepository.save(subscription);

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/subscriptions/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> unsubscribe(Principal principal,  @PathVariable Long id) {

        validateSubscriptionAccess(principal, id);

        // EmptyResultDataAccessException is thrown (managed by SubscriptionControllerAdvice) if subscription does not exist.
        subscriptionRepository.delete(id);
        return ResponseEntity.noContent().build();
    }

    private ApiUser getUser(String username) {
        Optional<ApiUser> apiUser = this.apiUserRepository.findByUsername(username);
        if (apiUser.isPresent()) {
            return apiUser.get();
        } else {
            throw new UserNotFoundException(username);
        }
    }

    private Subscription validateSubscriptionAccess(Principal principal, Long id) {
        Subscription subscription = this.subscriptionRepository.findOne(id);
        if (subscription == null) {
            throw new SubscriptionNotFoundException(id);
        } else if (!principal.getName().equals(subscription.getUser().getUsername())) {
            throw new ForbiddenException(id);
        }
        return subscription;
    }


    private void validateSubscriptionParam(Subscription subscription) {
        if (subscription == null || subscription.getBirthDate() == null ||
            subscription.getEmail() == null || subscription.getEmail().isEmpty() ||
            subscription.getNewsletterId() == null || subscription.getNewsletterId().isEmpty()) {
                throw new BadRequestException();
        }
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Gender.class, new GenderEnumConverter());
    }
}
