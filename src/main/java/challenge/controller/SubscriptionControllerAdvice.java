package challenge.controller;

import challenge.exception.BadRequestException;
import challenge.exception.ForbiddenException;
import challenge.exception.SubscriptionNotFoundException;
import challenge.exception.UserNotFoundException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.VndErrors;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class SubscriptionControllerAdvice {

    @ResponseBody
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    VndErrors userNotFoundExceptionHandler(UserNotFoundException ex) {
        return new VndErrors("error", ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    VndErrors badRequestExceptionHandler(BadRequestException ex) {
        return new VndErrors("error", ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    VndErrors constraintViolationExceptionHandler(ConstraintViolationException ex) {
        return new VndErrors("error", "Provided email is already subscribed to specified newsletter.");
    }

    @ResponseBody
    @ExceptionHandler(SubscriptionNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    VndErrors subscriptionNotFoundExceptionHandler(SubscriptionNotFoundException ex) {
        return new VndErrors("error", ex.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(EmptyResultDataAccessException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    VndErrors subscriptionNotFoundExceptionHandler(EmptyResultDataAccessException ex) {
        return new VndErrors("error", "Specified subscription does not exist.");
    }

    @ResponseBody
    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    VndErrors subscriptionNotFoundExceptionHandler(ForbiddenException ex) {
        return new VndErrors("error", ex.getMessage());
    }

}
