package challenge.converter;

import challenge.model.Gender;

import java.beans.PropertyEditorSupport;

public class GenderEnumConverter extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        String capitalized = text.toUpperCase();
        Gender gender = Gender.valueOf(capitalized);
        setValue(gender);
    }
}
