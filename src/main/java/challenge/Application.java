package challenge;

import challenge.model.ApiUser;
import challenge.model.Gender;
import challenge.model.Role;
import challenge.model.Subscription;
import challenge.repository.ApiUserRepository;
import challenge.repository.SubscriptionRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }


    @Bean
    CommandLineRunner init(SubscriptionRepository subscriptionRepository, ApiUserRepository apiUserRepository) {
        return new CommandLineRunner() {
            @Override
            public void run(String... strings) throws Exception {
                ApiUser apiUser1 = apiUserRepository.save(new ApiUser("testUser1", "password1", Role.USER));
                ApiUser apiUser2 = apiUserRepository.save(new ApiUser("testUser2", "password2", Role.USER));

                subscriptionRepository.save(new Subscription("testUser1@gmail.com", "Test1", Gender.MALE, new Date(),
                        true, "news1", apiUser1));
                subscriptionRepository.save(new Subscription("testUser1-B@gmail.com", "Test1", Gender.MALE, new Date(),
                        true, "news2", apiUser1));
                subscriptionRepository.save(new Subscription("testUser2@gmail.com", "Test2", Gender.FEMALE, new Date(),
                        true, "news3", apiUser2));
            }
        };
    }
}
