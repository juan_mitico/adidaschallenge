package challenge.repository;

import challenge.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {

        Collection<Subscription> findByUser_Username(String username);
}
