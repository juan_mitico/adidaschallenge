package challenge.repository;

import challenge.model.ApiUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ApiUserRepository extends JpaRepository<ApiUser, Long> {

        Optional<ApiUser> findByUsername(String username);

}
