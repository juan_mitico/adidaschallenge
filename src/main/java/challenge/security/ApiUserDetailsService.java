package challenge.security;

import challenge.model.ApiUser;
import challenge.repository.ApiUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

public class ApiUserDetailsService implements UserDetailsService {

    @Autowired
    private ApiUserRepository apiUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<ApiUser> apiUser = apiUserRepository.findByUsername(username);
        if (apiUser.isPresent()) {
            return new User(apiUser.get().username, apiUser.get().password, AuthorityUtils.createAuthorityList(apiUser.get().getRole().name()));
        } else {
            throw new UsernameNotFoundException("Could not find the user '" + username + "'");
        }
    }
}
