package challenge.resource;

import challenge.controller.SubscriptionController;
import challenge.model.Subscription;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class SubscriptionResource extends ResourceSupport {

    private final Subscription subscription;

    public SubscriptionResource(Subscription subscription) {
        this.subscription = subscription;
        this.add(linkTo(methodOn(SubscriptionController.class)
                .getAllSubscriptions(null)).withRel("subscriptions"));
        this.add(linkTo(methodOn(SubscriptionController.class)
                .getSubscription(null, subscription.getId())).withSelfRel());
    }

    public Subscription getSubscription() {
        return subscription;
    }
}