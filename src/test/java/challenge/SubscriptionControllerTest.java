package challenge;

import challenge.model.ApiUser;
import challenge.model.Gender;
import challenge.model.Role;
import challenge.model.Subscription;
import challenge.repository.ApiUserRepository;
import challenge.repository.SubscriptionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.Filter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SubscriptionControllerTest {

    private static class AuthBody {
        public String access_token;
        public String token_type;
        public String expires_in;
        public String scope;
    }

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    private MockMvc mockMvc;

    @Autowired private WebApplicationContext webApplicationContext;
    @Autowired private Filter springSecurityFilterChain;

    @Autowired private SubscriptionRepository subscriptionRepository;
    @Autowired private ApiUserRepository apiUserRepository;

    private ApiUser apiUser1;
    private ApiUser apiUser2;
    private List<Subscription> subscriptionList = new ArrayList<>();
    private AuthBody authenticationData;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.stream(converters)
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("The JSON message converter must not be null", this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .addFilters(springSecurityFilterChain)
                .build();

        this.subscriptionRepository.deleteAllInBatch();
        this.apiUserRepository.deleteAllInBatch();

        this.apiUser1 = apiUserRepository.save(new ApiUser("Juan", "password", Role.USER));
        this.apiUser2 = apiUserRepository.save(new ApiUser("Pedro", "password", Role.USER));

        this.subscriptionList.add(subscriptionRepository.save(new Subscription("juan@gmail.com",
                "Juan", Gender.MALE, new Date(), true, "newsId1", apiUser1)));
        this.subscriptionList.add(subscriptionRepository.save(new Subscription("jgomez@hotmail.com",
                "Juan", Gender.MALE, new Date(), true, "newsId2", apiUser1)));
        this.subscriptionList.add(subscriptionRepository.save(new Subscription("pedro@gmail.com",
                "Pedro", Gender.MALE, new Date(), true, "newsId2", apiUser2)));

        // Get authentication token for user "Juan" to be used in tests
        MvcResult result = mockMvc.perform(post("/oauth/token").with(user("challenge-client"))
                .param("username", apiUser1.getUsername())
                .param("password", apiUser1.getPassword())
                .param("grant_type", "password"))
                .andExpect(status().isOk())
                .andDo(print()).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        authenticationData = mapper.readValue(result.getResponse().getContentAsString(), AuthBody.class);
    }

    @Test
    public void getBookmarkTest() throws Exception {
        mockMvc.perform(get("/subscriptions/" + this.subscriptionList.get(0).getId())
                .header("Authorization", "Bearer " + authenticationData.access_token))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.subscription.id").value(this.subscriptionList.get(0).getId()))
                .andExpect(jsonPath("$.subscription.email").value(this.subscriptionList.get(0).getEmail()));
    }

    @Test
    public void getBookmarksTest() throws Exception {
        mockMvc.perform(get("/subscriptions")
                .header("Authorization", "Bearer " + authenticationData.access_token))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.content", hasSize(2)))
                .andExpect(jsonPath("$.content[0].subscription.id").value(this.subscriptionList.get(0).getId()))
                .andExpect(jsonPath("$.content[1].subscription.id").value(this.subscriptionList.get(1).getId()));
    }

    @Test
    public void subscribeTest() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        String subscriptionJson = objectMapper.writeValueAsString(new Subscription("juan_gomez@gmail.com", "Juan",
                Gender.MALE, new Date(), true, "newsId6", apiUser2));

        mockMvc.perform(post("/subscriptions")
                .contentType(contentType)
                .content(subscriptionJson)
                .header("Authorization", "Bearer " + authenticationData.access_token))
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    public void updateSubscriptionTest() throws Exception {

        final String newEmail = "juana@gmail.com";
        final String newName = "juana";
        final Gender newGender = Gender.FEMALE;
        final String newNewsletterId = "newsId7";

        Subscription subscription = new Subscription(newEmail, newName, newGender, subscriptionList.get(0).getBirthDate(),
                subscriptionList.get(0).isConsent(), newNewsletterId, subscriptionList.get(0).getUser());

        ObjectMapper objectMapper = new ObjectMapper();
        String subscriptionJson = objectMapper.writeValueAsString(subscription);

        mockMvc.perform(put("/subscriptions/" + subscriptionList.get(0).getId())
                .contentType(contentType)
                .content(subscriptionJson)
                .header("Authorization", "Bearer " + authenticationData.access_token))
                .andDo(print())
                .andExpect(status().isOk());

        Subscription updatedSubscription = subscriptionRepository.findOne(subscriptionList.get(0).getId());

        assertEquals(updatedSubscription.getEmail(), newEmail);
        assertEquals(updatedSubscription.getFirstName(), newName);
        assertEquals(updatedSubscription.getGender(), newGender);
        assertEquals(updatedSubscription.getNewsletterId(), newNewsletterId);
    }

    @Test
    public void unsubscribeTest() throws Exception {

        mockMvc.perform(delete("/subscriptions/" + subscriptionList.get(0).getId())
                .header("Authorization", "Bearer " + authenticationData.access_token))
                .andDo(print())
                .andExpect(status().isNoContent());

        assertNull(subscriptionRepository.findOne(subscriptionList.get(0).getId()));
    }

    @Test
    public void badCredentialsTest() throws Exception {

        mockMvc.perform(post("/oauth/token").with(user("challenge-client"))
                .param("username", apiUser1.getUsername())
                .param("password", "badPassword")
                .param("grant_type", "password"))
                .andExpect(status().isBadRequest())
                .andDo(print()).andReturn();
    }

    @Test
    public void forbiddenTest() throws Exception {

        // ApiUser1 does not have access to subscription created by ApiUser2
        mockMvc.perform(get("/subscriptions/" + this.subscriptionList.get(2).getId())
                .header("Authorization", "Bearer " + authenticationData.access_token))
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    public void conflictTest() throws Exception {

        // Try to update subscription 0 to have the same email and newLetterId than subscription 1.

        Subscription subscription = new Subscription(subscriptionList.get(1).getEmail(), subscriptionList.get(0).getFirstName(),
                subscriptionList.get(0).getGender(), subscriptionList.get(0).getBirthDate(),
                subscriptionList.get(0).isConsent(), subscriptionList.get(1).getNewsletterId(), subscriptionList.get(0).getUser());

        ObjectMapper objectMapper = new ObjectMapper();
        String subscriptionJson = objectMapper.writeValueAsString(subscription);

        mockMvc.perform(put("/subscriptions/" + subscriptionList.get(0).getId())
                .contentType(contentType)
                .content(subscriptionJson)
                .header("Authorization", "Bearer " + authenticationData.access_token))
                .andDo(print())
                .andExpect(status().isConflict());
    }

}
