# Coding Challenge: web services and integration

## Application description

Developed application simulates a standalone server application which exposes a REST API to create, update and delete email subscriptions to different newsletters.

Access to the API is secure and users must be authorized by the server prior to make any subscription request.

The application maintains an in-memory database where users and subscriptions are stored. For now, only subscription-related operations are implemented, and no user management is available.

When applications starts, it creates automatically several users and subscriptions in order to test subscription operations exposed by the REST API.

Event Service and Email Service components have not been considered and they are not implemented.

Available operations are:

* Subscribe (create subscription)
* Unsubscribe (cancel subscription)
* Update subscription data (email, gender, birth date, consent and newsletter)
* Get all user subscriptions
* Get a certain user subscription

**Users can only access to subscriptions created by themselves.** In this way, if user requests all subscriptions, only those which have been created by the user are retrieved.
Accordingly, only subscriptions property of the requester can be updated or deleted.

## Frameworks / Libraries

Following frameworks and libraries have been used to implement the application:

* Spring: provides all infrastructure to implement quickly the following features:
	- Standalone server application (SpringBoot)
	- Database access (Spring Data JPA)
	- Secured REST API (Spring Web, Spring Security), using HATEOAS links (Spring Hateoas)
* H2 Database: used to maintain an in-memory database.
* JsonPath: used in tests to check service responses correctness.

## Application usage

### Build

Execute the following command to compile, run tests and package the application into a .jar file:

	$ mvn package

#### HTTP / HTTPS

By default, application does not use HTTPS. If we want to use it, we must uncomment the following lines in configuration file `src/main/resources/application.properties` before building the project.

```
# Configure the server to run with SSL/TLS and using HTTPS
server.port = 8443
server.ssl.key-store = classpath:tomcat.keystore
server.ssl.key-store-password = password
server.ssl.key-password = password
```

**If HTTPS is enabled, all references to `http://localhost:8080` below must be replaced by `https://localhost:8443`**

### Run

Execute the following command to run the application:

	$ java -jar target/challenge-1.0.jar
	
### Use

#### Authorization

Users must be authorized by the server application prior to make subscription requests. Only users stored in application database can be authorized.

User must request an authorization token providing its user and password. If credentials are valid, service response contains a token that must be included in all subscription requests.

Application provides the following endpoint for authorization requests:

	http://localhost:8080/oauth/token

Example:

	curl -X POST -vu challenge-client: http://localhost:8080/oauth/token -H "Accept: application/json" -d "username=testUser1&password=password1&grant_type=password"
	
`challenge-client` is the client ID which the application security has been configured for (no password needed).

Username, password and grant type must be specified. Authorization will fail otherwise (status code 400).

If authorization succeeds, user will get an access token:

	{"access_token":"b0ba2c9e-4041-4bdf-af28-63c282c5da0c","token_type":"bearer","expires_in":43199,"scope":"read write"}
	
**NOTE**: To make authorization process secure, HTTPS must be enabled (see 'Build' section) in order to avoid sending plain username/password.
	
#### Subscription requests

Once authorized, user is ready to make subscription requests using its authorization token. See the following examples.

**Get all user subscriptions**

	curl -v http://127.0.0.1:8080/subscriptions -H "Authorization: Bearer b0ba2c9e-4041-4bdf-af28-63c282c5da0c"
	
Response:
```
HTTP/1.1 200
```
```json
{
	"links" : [],
	"content" : [{
		"subscription" : {
			"id" : 1,
			"email" : "testUser1@gmail.com",
			"firstName" : "Test1",
			"gender" : "MALE",
			"birthDate" : "15-01-2017",
			"consent" : true,
			"newsletterId" : "news1"
		},
		"links" : [{
			"rel" : "subscriptions",
			"href" : "http://127.0.0.1:8080/subscriptions"
		}, {
			"rel" : "self",
			"href" : "http://127.0.0.1:8080/subscriptions/1"
		}]
	}, {
		"subscription" : {
			"id" : 2,
			"email" : "testUser1-B@gmail.com",
			"firstName" : "Test1",
			"gender" : "MALE",
			"birthDate" : "15-01-2017",
			"consent" : true,
			"newsletterId" : "news2"
		},
		"links" : [{
			"rel" : "subscriptions",
			"href" : "http://127.0.0.1:8080/subscriptions"
		}, {
			"rel" : "self",
			"href" : "http://127.0.0.1:8080/subscriptions/2"
		}]
	}]
}
```

Response only contains subscriptions related to `testUser1`

**Get a certain user subscription**

	curl -v http://127.0.0.1:8080/subscriptions/1 -H "Authorization: Bearer 061c8114-bcf6-4abd-b80e-3918a366a548"
	
Response:
```
HTTP/1.1 200
```
```json
{
	"subscription" : {
		"id" : 1,
		"email" : "testUser1@gmail.com",
		"firstName" : "Test1",
		"gender" : "MALE",
		"birthDate" : "15-01-2017",
		"consent" : true,
		"newsletterId" : "news1"
	},
	"links" : [{
			"rel" : "subscriptions",
			"href" : "http://127.0.0.1:8080/subscriptions"
		}, {
			"rel" : "self",
			"href" : "http://127.0.0.1:8080/subscriptions/1"
		}
	]
}
```

**Create subscription**

	curl -v -X POST http://127.0.0.1:8080/subscriptions -H "Content-Type: application/json" -H "Authorization: Bearer 061c8114-bcf6-4abd-b80e-3918a366a548" -d '{"gender":"MALE", "email":"testUser1@hotmail.com", "newsletterId":"1234", "birthDate":"02-11-1988"}'
	 
Response contains a link to the created resource, and includes the ID of the new subscription in its body (4):

```
HTTP/1.1 201
Location: http://127.0.0.1:8080/subscriptions/4
```

**Update subscription**

	curl -v -X PUT http://127.0.0.1:8080/subscriptions/4 -H "Content-Type: application/json" -H "Authorization: Bearer 061c8114-bcf6-4abd-b80e-3918a366a548" -d '{"gender":"FEMALE", "email":"testUser1@yahoo.com", "newsletterId":"666", "birthDate":"02-11-1989"}'
	
Response:
```
HTTP/1.1 200
```

**Delete subscription**

	curl -v -X DELETE http://127.0.0.1:8080/subscriptions/4 -H "Authorization: Bearer 061c8114-bcf6-4abd-b80e-3918a366a548"

Response:
	
```
HTTP/1.1 204
```

**Try to access a subscription from other user**

	curl -v http://127.0.0.1:8080/subscriptions/3 -H "Authorization: Bearer 061c8114-bcf6-4abd-b80e-3918a366a548"
	
Response:
```
HTTP/1.1 403
```
```json
[{"logref":"error","message":"Forbidden access to subscription '3'","links":[]}]
```

This occurs also when trying to delete or update a subscription from other user.

**Try to subscribe an email to a newsletter that is already subscribed**

Request 1:

	curl -v -X POST http://127.0.0.1:8080/subscriptions -H "Content-Type: application/json" -H "Authorization: Bearer 061c8114-bcf6-4abd-b80e-3918a366a548" -d '{"gender":"MALE", "email":"testUser1@hotmail.com", "newsletterId":"1234", "birthDate":"02-11-1988"}'

Response 1:

```
HTTP/1.1 201
Location: http://127.0.0.1:8080/subscriptions/4
```

Request 2:

	curl -v -X POST http://127.0.0.1:8080/subscriptions -H "Content-Type: application/json" -H "Authorization: Bearer 061c8114-bcf6-4abd-b80e-3918a366a548" -d '{"gender":"MALE", "email":"testUser1@hotmail.com", "newsletterId":"1234", "birthDate":"02-11-1988"}'

Response 2:

```
HTTP/1.1 409
```
```json
[{"logref":"error","message":"Provided email is already subscribed to specified newsletter.","links":[]}]
```
